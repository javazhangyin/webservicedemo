package com.example.webservicedemo;

import com.example.webservicedemo.common.StaticParam;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.headers.Header;
import org.apache.cxf.helpers.DOMUtils;
import org.apache.cxf.message.Message;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.namespace.QName;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dyf
 * @create 2021/3/22 15:34
 */
public class Test {


    public static void main(String[] args) {
//
        List<Header> headers = new ArrayList<>();
//        Document document = DOMUtils.createDocument();
//        Element authorEle = document.createElement("author");
//        authorEle.setAttribute("username", "USERNAME");
//        authorEle.setAttribute("password", "PASSWORD");
//        headers.add(new Header(new QName(""), authorEle));
//
//        System.out.println(document);

//        Document document = DOMUtils.createDocument();
//        Element authorEle = document.createElement("author");
//        authorEle.setAttribute("username", "11");
//        authorEle.setAttribute("password", "22");


        Document doc = DOMUtils.createDocument();
        Element auth = doc.createElementNS(StaticParam.TARGETNAMESPACE,"SecurityHeader");
        Element UserName = doc.createElement("username");
        Element UserPass = doc.createElement("password");

        UserName.setTextContent("111");
        UserPass.setTextContent("password");

        auth.appendChild(UserName);
        auth.appendChild(UserPass);

        doc.appendChild(auth);
        System.out.println(toStringFromDoc(doc));










    }



    /*
     * 把dom文件转换为xml字符串
     */
    public static String toStringFromDoc(Document document) {
        String result = null;

        if (document != null) {
            StringWriter strWtr = new StringWriter();
            StreamResult strResult = new StreamResult(strWtr);
            TransformerFactory tfac = TransformerFactory.newInstance();
            try {
                javax.xml.transform.Transformer t = tfac.newTransformer();
                t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                t.setOutputProperty(OutputKeys.INDENT, "yes");
                t.setOutputProperty(OutputKeys.METHOD, "xml"); // xml, html,
                // text
                t.setOutputProperty(
                        "{http://xml.apache.org/xslt}indent-amount", "4");
                t.transform(new DOMSource(document.getDocumentElement()),
                        strResult);
            } catch (Exception e) {
                System.err.println("XML.toString(Document): " + e);
            }
            result = strResult.getWriter().toString();
            try {
                strWtr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }



}

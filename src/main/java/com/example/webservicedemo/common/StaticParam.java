package com.example.webservicedemo.common;

/**
 * @author dyf
 * @create 2021/3/22 18:02
 */
public class StaticParam {
    public static final String TARGETNAMESPACE = "http://tempuri.org/";
//    public static final String TARGETNAMESPACE = "http://impl.service.webservicedemo.example.com";

    public static final String MYSOAPHEADER = "MySoapHeader";
}

package com.example.webservicedemo.interceptor;

import cn.hutool.json.JSONUtil;
import com.example.webservicedemo.bean.MyResponseResult;
import org.apache.cxf.binding.soap.SoapHeader;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.headers.Header;
import org.apache.cxf.helpers.DOMUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.MessageContentsList;
import org.apache.cxf.message.XMLMessage;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.opensaml.soap.common.SOAPException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

/**
 * @author dyf
 * @create 2021/3/23 10:50
 */
@Component
public class ValidateInterceptor extends AbstractPhaseInterceptor<SoapMessage> {


    private static final String USERNAME = "user";
    private static final String PASSWORD = "123";

    public ValidateInterceptor(String phase) {
        super(phase);
    }

    public ValidateInterceptor() {
        super(Phase.PRE_INVOKE);
    }

    @Override
    public void handleMessage(SoapMessage soapMessage) throws Fault {

        try {


            List<Header> headers = soapMessage.getHeaders();
            String username = null;
            String password = null;
            if (headers == null || headers.isEmpty()) {

                retrunCustomMsg("找不到Header，无法验证用户信息",soapMessage);
                return;


                //throw new Fault(new IllegalArgumentException("找不到Header，无法验证用户信息"));
            }
            //获取用户名,密码
            for (Header header : headers) {
                SoapHeader soapHeader = (SoapHeader) header;
                Element e = (Element) soapHeader.getObject();
                NodeList usernameNode = e.getElementsByTagName("username");
                NodeList pwdNode = e.getElementsByTagName("password");
                if (usernameNode.getLength()>0) {
                    username=usernameNode.item(0).getTextContent();
                }
                if (pwdNode.getLength()>0) {
                    password=pwdNode.item(0).getTextContent();
                }

                if( StringUtils.isEmpty(username)||StringUtils.isEmpty(password)){
                    //throw new Fault(new IllegalArgumentException("用户信息为空"));
                    retrunCustomMsg("用户信息为空",soapMessage);
                    return;
                }
            }
            // 校验用户名密码
            if (!(USERNAME.equals(username) && PASSWORD.equals(password))) {
                retrunCustomMsg("认证失败,用户认证信息错误!",soapMessage);
                return;
//                SOAPException soapExc = new SOAPException("认证失败");
//                System.err.println("用户认证信息错误");
//                throw new Fault(soapExc);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * 封装错误自定义信息
     * @param msg
     * @param soapMessage
     */
    private void retrunCustomMsg(String msg,SoapMessage soapMessage){
        HttpServletResponse response  = (HttpServletResponse)soapMessage.get(AbstractHTTPDestination.HTTP_RESPONSE);

        ////javax.xml.ws.wsdl.operation
        QName qname = (QName)soapMessage.get("javax.xml.ws.wsdl.operation");
        try {
            ServletOutputStream out = response.getOutputStream();

            String outMsg = JSONUtil.toJsonStr(new MyResponseResult().error(msg));
            String docStr = getDocStr(qname.getLocalPart(),outMsg);
            out.write(docStr.getBytes("utf-8"));
            out.flush();
            soapMessage.getInterceptorChain().doInterceptStartingAfter(soapMessage, "org.apache.cxf.jaxrs.interceptor.JAXRSOutInterceptor");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getDocStr(String tagStr,String jsonStr) {

        Document document = DOMUtils.createDocument();

        Element envelope = document.createElementNS("http://schemas.xmlsoap.org/soap/envelope/","soap:Envelope");
        Element body = document.createElement("soap:Body");
        envelope.appendChild(body);
        Element tag = document.createElement(tagStr);
        body.appendChild(tag);
        Element retrun  = document.createElement("return");
        retrun.setTextContent(jsonStr);
        tag.appendChild(retrun);

        document.appendChild(envelope);


       return toStringFromDoc(document);

    }



    /*
     * 把dom文件转换为xml字符串
     */
    private static String toStringFromDoc(Document document) {
        String result = null;

        if (document != null) {
            StringWriter strWtr = new StringWriter();
            StreamResult strResult = new StreamResult(strWtr);
            TransformerFactory tfac = TransformerFactory.newInstance();
            try {
                javax.xml.transform.Transformer t = tfac.newTransformer();
                t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                t.setOutputProperty(OutputKeys.INDENT, "yes");
                t.setOutputProperty(OutputKeys.METHOD, "xml"); // xml, html,
                // text
                t.setOutputProperty(
                        "{http://xml.apache.org/xslt}indent-amount", "4");
                t.transform(new DOMSource(document.getDocumentElement()),
                        strResult);
            } catch (Exception e) {
                System.err.println("XML.toString(Document): " + e);
            }
            result = strResult.getWriter().toString();
            try {
                strWtr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

}
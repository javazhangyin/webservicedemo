package com.example.webservicedemo.service;

import com.example.webservicedemo.bean.Student;
import com.example.webservicedemo.common.StaticParam;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Map;

/**
 *
 * @Project :
 * @Package : com.example.webservicedemo.service
 * @Class : StudentService
 * @Description :
 * @author : javazhangyin
 * @CreateDate : 2021-03-24 10:09:50
 * @version : V1.0.0
 * @Copyright : javazhang All rights reserved..
 * @Reviewed :
 * @UpateLog :    Name    Date    Reason/Contents
 *             ---------------------------------------
 *                ****    ****    ****
 *
 */
@WebService(targetNamespace = StaticParam.TARGETNAMESPACE)
public interface StudentService {

    /**
     *
     * @Method : getStudentAge
     * @Description :
     * @param name :
     * @return : java.lang.Integer
     * @author : javazhangyin
     * @CreateDate : 2021-03-24 08:57:36
     *
     */
    @WebMethod
    public Integer getStudentAge(@WebParam(name = "name",targetNamespace = StaticParam.TARGETNAMESPACE) String name);

    @WebMethod
    public String getStudentName();

    @WebMethod
    public String getOneStudentInfo();

    @WebMethod
    public String getAllStudentsInfo();

    @WebMethod
    public String getSchoolInfo();

    @WebMethod
    public String getMyResponseResult();
    /**
     *
     * @Method : VaildUserLogin
     * @Description :
     * @param userCode :
     * @param pwd :
     * @param deviceId :
     * @return : java.lang.String
     * @author : javazhangyin
     * @CreateDate : 2021-03-24 08:57:06
     *
     */
    public String VaildUserLogin(@WebParam(name = "userCode",targetNamespace = StaticParam.TARGETNAMESPACE)String userCode,
                                 @WebParam(name = "pwd",targetNamespace = StaticParam.TARGETNAMESPACE)String pwd,
                                 @WebParam(name = "deviceId",targetNamespace = StaticParam.TARGETNAMESPACE)String deviceId);

    /**
     *
     * @Method : GetFinishAffairs
     * @Description :
     * @param userCode :
     * @param page :
     * @param pageSize :
     * @return : java.lang.String
     * @author : javazhangyin
     * @CreateDate : 2021-03-24 10:03:35
     *
     */
    public String GetFinishAffairs(@WebParam(name = "userCode",targetNamespace = StaticParam.TARGETNAMESPACE)String userCode,
                                   @WebParam(name = "page",targetNamespace = StaticParam.TARGETNAMESPACE)String page,
                                   @WebParam(name = "pageSize",targetNamespace = StaticParam.TARGETNAMESPACE)String pageSize);


}

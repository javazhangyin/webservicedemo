package com.example.webservicedemo.service.impl;

import cn.hutool.json.JSONUtil;
import com.example.webservicedemo.bean.FinishAffairs;
import com.example.webservicedemo.bean.MyResponseResult;
import com.example.webservicedemo.bean.Student;
import com.example.webservicedemo.common.StaticParam;
import com.example.webservicedemo.service.StudentService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import javax.jws.WebService;
import java.util.*;

/**
 *
 * @Project :
 * @Package : com.example.webservicedemo.service.impl
 * @Class : StudentServiceImpl
 * @Description :
 * @author : javazhangyin
 * @CreateDate : 2021-03-24 10:02:46
 * @version : V1.0.0
 * @Copyright : javazhang All rights reserved..
 * @Reviewed :
 * @UpateLog :    Name    Date    Reason/Contents
 *             ---------------------------------------
 *                ****    ****    ****
 *
 */
@Service
@WebService(serviceName = "StudentService", // 服务名
        targetNamespace = StaticParam.TARGETNAMESPACE, // 实现类包名倒写
        endpointInterface = "com.example.webservicedemo.service.StudentService") // 接口的全路径
public class StudentServiceImpl implements StudentService {

    @Override
    public Integer getStudentAge(String name) {
        return new Student(name, 18, "男").getAge();
    }

    @Override
    public String getStudentName() {
        MyResponseResult result = new MyResponseResult();

        return ResultToJsonString("孙悟空");
    }

    @Override
    public String getOneStudentInfo() {
        MyResponseResult result = new MyResponseResult();
        Student stu = new Student("紫霞仙子", 18, "女");

        return ResultToJsonString(stu);
    }

    @Override
    public String getAllStudentsInfo() {
        MyResponseResult result = new MyResponseResult();
        List<Student> stus = new ArrayList<>();
        Student stu1 = new Student("唐三藏", 20, "男");
        Student stu2 = new Student("孙悟空", 20, "男");
        Student stu3 = new Student("猪八戒", 20, "男");
        Student stu4 = new Student("沙和尚", 20, "男");
        stus.add(stu1);
        stus.add(stu2);
        stus.add(stu3);
        stus.add(stu4);
        return ResultToJsonString(stus);
    }

    @Override
    public String getSchoolInfo() {
        MyResponseResult result = new MyResponseResult();
        Map<String, Object> school = new HashMap<>();
        school.put("name", "XX大学");
        school.put("location", "广东");
        return ResultToJsonString(school);
    }

    @Override
    public String getMyResponseResult() {
        MyResponseResult result = new MyResponseResult();
        Map<String, Object> school = new HashMap<>();
        school.put("name", "XX大学");
        school.put("location", "广东");

       return ResultToJsonString(school);
        //return JSONUtil.toJsonStr(success);
    }

    @Override
    public String VaildUserLogin(String userCode, String pwd, String deviceId) {
        return "{\"IsError\":false,\"Message\":\"success\",\"Data\":[{\"maintitle\":\"测试1\",\"recid\":\"2021032313580980501759353513\",\"security_class\":null,\"doc_no\":null,\"input_user\":\"admin\",\"jinji\":1,\"modify_flag\":null,\"modify_date\":null,\"oldstate\":null,\"design_flowid\":null,\"created_date\":null,\"responsibility\":null,\"state\":1,\"del_user\":null,\"input_date\":\"2021-03-23 13:59:05\",\"modify_user\":null,\"input_flag\":\"T\",\"is_unit_open\":null},{\"maintitle\":\"个人事务\",\"recid\":\"2021031918535111002026925063\",\"security_class\":null,\"doc_no\":null,\"input_user\":\"admin\",\"jinji\":2,\"modify_flag\":null,\"modify_date\":null,\"oldstate\":null,\"design_flowid\":null,\"created_date\":null,\"responsibility\":null,\"state\":1,\"del_user\":null,\"input_date\":\"2021-03-19 18:53:51\",\"modify_user\":null,\"input_flag\":\"T\",\"is_unit_open\":null},{\"maintitle\":\"附言区测试\",\"recid\":\"2021031009033830761723926464\",\"security_class\":null,\"doc_no\":null,\"input_user\":\"admin\",\"jinji\":1,\"modify_flag\":null,\"modify_date\":null,\"oldstate\":null,\"design_flowid\":null,\"created_date\":null,\"responsibility\":null,\"state\":1,\"del_user\":null,\"input_date\":\"2021-03-10 09:03:38\",\"modify_user\":null,\"input_flag\":\"T\",\"is_unit_open\":null},{\"maintitle\":\"感谢信\",\"recid\":\"2021030517572602781643570713\",\"security_class\":null,\"doc_no\":null,\"input_user\":\"admin\",\"jinji\":8,\"modify_flag\":null,\"modify_date\":null,\"oldstate\":null,\"design_flowid\":null,\"created_date\":null,\"responsibility\":null,\"state\":1,\"del_user\":null,\"input_date\":\"2021-03-05 17:57:26\",\"modify_user\":null,\"input_flag\":\"T\",\"is_unit_open\":null},{\"maintitle\":\"测试管理哈哈哈哈哈哈哈\",\"recid\":\"2021022509264966191019145613\",\"security_class\":null,\"doc_no\":null,\"input_user\":\"admin\",\"jinji\":2,\"modify_flag\":null,\"modify_date\":null,\"oldstate\":null,\"design_flowid\":null,\"created_date\":null,\"responsibility\":null,\"state\":1,\"del_user\":null,\"input_date\":\"2021-02-25 09:26:49\",\"modify_user\":null,\"input_flag\":\"T\",\"is_unit_open\":null},{\"maintitle\":\"测试发布\",\"recid\":\"2021022509254303331359309001\",\"security_class\":null,\"doc_no\":null,\"input_user\":\"admin\",\"jinji\":2,\"modify_flag\":null,\"modify_date\":null,\"oldstate\":null,\"design_flowid\":null,\"created_date\":null,\"responsibility\":null,\"state\":1,\"del_user\":null,\"input_date\":\"2021-02-25 09:25:43\",\"modify_user\":null,\"input_flag\":\"T\",\"is_unit_open\":null},{\"maintitle\":\"所有事物22222\",\"recid\":\"2021022410515655681741151208\",\"security_class\":null,\"doc_no\":null,\"input_user\":\"admin\",\"jinji\":1,\"modify_flag\":null,\"modify_date\":null,\"oldstate\":null,\"design_flowid\":null,\"created_date\":null,\"responsibility\":null,\"state\":1,\"del_user\":null,\"input_date\":\"2021-02-24 10:51:56\",\"modify_user\":null,\"input_flag\":\"T\",\"is_unit_open\":null},{\"maintitle\":\"1111\",\"recid\":\"2021022409334351441776204089\",\"security_class\":null,\"doc_no\":null,\"input_user\":\"admin\",\"jinji\":1,\"modify_flag\":null,\"modify_date\":null,\"oldstate\":null,\"design_flowid\":null,\"created_date\":null,\"responsibility\":null,\"state\":1,\"del_user\":null,\"input_date\":\"2021-02-24 09:33:43\",\"modify_user\":null,\"input_flag\":\"T\",\"is_unit_open\":null},{\"maintitle\":\"0224测试撤销\",\"recid\":\"2021022409213124741114905860\",\"security_class\":null,\"doc_no\":null,\"input_user\":\"admin\",\"jinji\":2,\"modify_flag\":null,\"modify_date\":null,\"oldstate\":null,\"design_flowid\":null,\"created_date\":null,\"responsibility\":null,\"state\":1,\"del_user\":null,\"input_date\":\"2021-02-24 09:21:31\",\"modify_user\":null,\"input_flag\":\"T\",\"is_unit_open\":null},{\"maintitle\":\"豆莹莹测试取回和撤销\",\"recid\":\"2021022409151354001402611679\",\"security_class\":null,\"doc_no\":null,\"input_user\":\"admin\",\"jinji\":1,\"modify_flag\":null,\"modify_date\":null,\"oldstate\":null,\"design_flowid\":null,\"created_date\":null,\"responsibility\":null,\"state\":1,\"del_user\":null,\"input_date\":\"2021-02-24 09:15:13\",\"modify_user\":null,\"input_flag\":\"T\",\"is_unit_open\":null}]}";
    }


    @Override
    public String GetFinishAffairs(String userCode, String page, String pageSize) {

        System.out.println("用户" + userCode);
        System.out.println("页码" + page);
        System.out.println("每页数量" + pageSize);

        int pageCount = Integer.parseInt(pageSize);
        List<FinishAffairs> list = new ArrayList<>();
        for (int i = 0; i < pageCount; i++) {
            FinishAffairs finishAffairs = new FinishAffairs();
            finishAffairs.setMaintitle("测试"+i);
            finishAffairs.setRecid(UUID.randomUUID().toString());
            finishAffairs.setInput_user("admin");
            finishAffairs.setJinji(1);
            finishAffairs.setState(1);
            finishAffairs.setInput_date(new Date());
            finishAffairs.setInput_flag("T");
            list.add(finishAffairs);
        }


        return ResultToJsonString(list);
    }

    /**
     *
     * @Method : ResultToJsonString
     * @Description : 封装返回方法
     * @param obj :
     * @return : java.lang.String
     * @author : javazhangyin
     * @CreateDate : 2021-03-24 10:02:14
     *
     */
    private String ResultToJsonString(Object obj){
        MyResponseResult result = new MyResponseResult();
        MyResponseResult success = result.success(obj);
        ObjectMapper mapper = new ObjectMapper();
        String res = null;
        try {
            res = mapper.writeValueAsString(success);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return res;
    }

}
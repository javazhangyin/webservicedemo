package com.example.webservicedemo.config;

import javax.xml.ws.Endpoint;
import com.example.webservicedemo.interceptor.AuthInterceptor;
import com.example.webservicedemo.interceptor.ValidateInterceptor;
import com.example.webservicedemo.service.StudentService;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CxfConfig {

    @Autowired
    private Bus bus;

    @Autowired
    private StudentService studentService;
    @Autowired
    AuthInterceptor authInterceptor;
    @Autowired
    ValidateInterceptor validateInterceptor;
    /**
     * studentService
     *
     * @return
     */
    @Bean
    public Endpoint studentServiceEndpoint() {
        EndpointImpl studentServiceEndpoint = new EndpointImpl(bus, studentService);
        studentServiceEndpoint.publish("/studentService");
//        studentServiceEndpoint.getInInterceptors().add(authInterceptor);
        studentServiceEndpoint.getInInterceptors().add(validateInterceptor);
        return studentServiceEndpoint;

    }
}

package com.example.webservicedemo.client;

import com.example.webservicedemo.bean.Student;
import com.example.webservicedemo.common.StaticParam;
import com.example.webservicedemo.service.StudentService;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.http.MediaType;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;
import java.util.Map;

/**
 * @author dyf
 * @create 2021/3/22 10:46
 */
public class CxfClientApplication2 {


    public static void main(String[] args) throws Exception{
        //获取wsdl文档资源地址
        URL wsdlDocumentLocation = new URL("http://localhost:8081/services/studentService?wsdl");
        //获取服务名称
        //1.namespaceURI - 命名空间地址
        //2.localPart - 服务视图名
        QName serviceName=new QName(StaticParam.TARGETNAMESPACE,"StudentService");
        Service service= Service.create(wsdlDocumentLocation, serviceName);
        //获取服务实现类
        StudentService mobileCodeWSSoap = service.getPort(StudentService.class);
        //调用方法
        Integer result  = mobileCodeWSSoap.getStudentAge("测试");
        System.out.println(result);
        String str = null;
    }


}

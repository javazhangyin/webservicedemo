package com.example.webservicedemo.client;

import com.example.webservicedemo.bean.Student;
import com.example.webservicedemo.interceptor.ClientCheckInterceptor;
import com.example.webservicedemo.service.StudentService;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

/**
 * @author dyf
 * @create 2021/3/22 10:46
 */
public class CxfClientApplication {
//还有JaxRs的方式，详细写法，请自行到CXF官网上查看demo

    public static void main(String[] args) {
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();//JaxWs方式，因为pom文件导的包是cxf-spring-boot-starter-jaxws

        factory.setServiceClass(StudentService.class);
        factory.setAddress("http://localhost:8081/services/studentService?wsdl");
        //添加用户名密码拦截器
        factory.getOutInterceptors().add(new ClientCheckInterceptor());
        StudentService service = (StudentService) factory.create();
        String name = service.getStudentName();
        System.out.println("student name:"+name);
        String stu = service.getOneStudentInfo();
        System.out.println("学生信息："+stu);
        System.out.println("调用带参数的："+service.getStudentAge("212"));

    }


}

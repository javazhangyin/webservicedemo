package com.example.webservicedemo.client;

import com.example.webservicedemo.service.StudentService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

/**
 * @author dyf
 * @create 2021/3/22 10:46
 */
public class CxfClientApplication3 {


    public static void main(String[] args) throws Exception{
        //获取wsdl文档资源地址  带wsdl的路径
        URL wsdlDocumentLocation = new URL("http://192.168.0.9:8091/WebService.asmx?WSDL");
        //获取服务名称
        //1.namespaceURI - 命名空间地址
        //2.localPart - 服务视图名
        QName serviceName=new QName("http://tempuri.org/","StudentService");
        Service service= Service.create(wsdlDocumentLocation, serviceName);
        //获取服务实现类
        StudentService mobileCodeWSSoap = service.getPort(StudentService.class);
        //调用方法
        Integer result  = mobileCodeWSSoap.getStudentAge("测试");
        System.out.println(result);
        String str = null;
    }


}

package com.example.webservicedemo.bean;

/**
 * @author dyf
 * @create 2021/3/23 10:18
 */
public class MyResponseResult {

    public Boolean IsError;
    public String Message;
    private Object Data;

    public Boolean getError() {
        return IsError;
    }

    public void setError(Boolean error) {
        IsError = error;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public Object getData() {
        return Data;
    }

    public void setData(Object data) {
        Data = data;
    }


    public MyResponseResult success(Object obj){
        this.Data = obj;
        this.Message = "success";
        this.IsError= false;
        return this;
    }
    public MyResponseResult error(){
        this.Data = null;
        this.Message = "fail";
        this.IsError= true;
        return this;
    }
    public MyResponseResult error(String msg){
        this.Data = null;
        this.Message = msg;
        this.IsError= true;
        return this;
    }
}

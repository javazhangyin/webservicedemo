package com.example.webservicedemo.bean;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * @author dyf
 * @create 2021/3/23 15:10
 */
public class FinishAffairs {


    private String maintitle;

    private String recid;

    private String security_class;

    private String doc_no;

    private String input_user;

    private int jinji;

    private String modify_flag;

    private String modify_date;

    private String oldstate;

    private String design_flowid;

    private String created_date;

    private String responsibility;

    private int state;

    private String del_user;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date input_date;

    private String modify_user;

    private String input_flag;

    private String is_unit_open;

    public void setMaintitle(String maintitle){
        this.maintitle = maintitle;
    }
    public String getMaintitle(){
        return this.maintitle;
    }
    public void setRecid(String recid){
        this.recid = recid;
    }
    public String getRecid(){
        return this.recid;
    }
    public void setSecurity_class(String security_class){
        this.security_class = security_class;
    }
    public String getSecurity_class(){
        return this.security_class;
    }
    public void setDoc_no(String doc_no){
        this.doc_no = doc_no;
    }
    public String getDoc_no(){
        return this.doc_no;
    }
    public void setInput_user(String input_user){
        this.input_user = input_user;
    }
    public String getInput_user(){
        return this.input_user;
    }
    public void setJinji(int jinji){
        this.jinji = jinji;
    }
    public int getJinji(){
        return this.jinji;
    }
    public void setModify_flag(String modify_flag){
        this.modify_flag = modify_flag;
    }
    public String getModify_flag(){
        return this.modify_flag;
    }
    public void setModify_date(String modify_date){
        this.modify_date = modify_date;
    }
    public String getModify_date(){
        return this.modify_date;
    }
    public void setOldstate(String oldstate){
        this.oldstate = oldstate;
    }
    public String getOldstate(){
        return this.oldstate;
    }
    public void setDesign_flowid(String design_flowid){
        this.design_flowid = design_flowid;
    }
    public String getDesign_flowid(){
        return this.design_flowid;
    }
    public void setCreated_date(String created_date){
        this.created_date = created_date;
    }
    public String getCreated_date(){
        return this.created_date;
    }
    public void setResponsibility(String responsibility){
        this.responsibility = responsibility;
    }
    public String getResponsibility(){
        return this.responsibility;
    }
    public void setState(int state){
        this.state = state;
    }
    public int getState(){
        return this.state;
    }
    public void setDel_user(String del_user){
        this.del_user = del_user;
    }
    public String getDel_user(){
        return this.del_user;
    }

    public void setInput_date(Date input_date){
        this.input_date = input_date;
    }
    public Date getInput_date(){
        return this.input_date;
    }
    public void setModify_user(String modify_user){
        this.modify_user = modify_user;
    }
    public String getModify_user(){
        return this.modify_user;
    }
    public void setInput_flag(String input_flag){
        this.input_flag = input_flag;
    }
    public String getInput_flag(){
        return this.input_flag;
    }
    public void setIs_unit_open(String is_unit_open){
        this.is_unit_open = is_unit_open;
    }
    public String getIs_unit_open(){
        return this.is_unit_open;
    }

}

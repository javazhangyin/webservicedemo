package com.example.webservicedemo.web;

import javax.jws.WebService;

/**
 * @author dyf
 * @create 2021/3/22 9:19
 */
@WebService(
        targetNamespace="http://tempuri.org/",
        portName="WeatherSOAPPort",
        serviceName="WeatherWSService",
        name="WeatherSOAP"
)
public class TestService {
}
